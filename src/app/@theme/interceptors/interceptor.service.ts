import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { AppURLS } from 'app/util/app.urls';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  appURLS = AppURLS;
  apiURL = environment.apiURL;
  constructor(public appStore: AppStore) {
  }
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('user'));
    let authReq = null;
    // Dont require Authorization in API headers for public urls
    if (this.appURLS.publicURLS.includes(req.url.split(this.apiURL)[1])) {
      authReq = req.clone({
        headers: new HttpHeaders({
        }),
      });
    } else  {
      const tokenString = 'Bearer ' + token.tokens['access-token'];
      authReq = req.clone({
        headers: new HttpHeaders({
          'Authorization': tokenString,
        }),
      });
    }
    return next.handle(authReq).pipe(
      map((event) => {
        return event;
      }),
    );
  }
}
