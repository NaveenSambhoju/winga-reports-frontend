import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'winga-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <winga-one-column-layout>
      <!-- <nb-menu [items]="menu"></nb-menu> -->
      <router-outlet></router-outlet>
    </winga-one-column-layout>
  `,
})
export class PagesComponent {
constructor() {}
  menu = MENU_ITEMS;
}
