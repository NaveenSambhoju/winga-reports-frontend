import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { DashboardService } from './dashboard.service';
@Injectable()
export class DateRangeResolver implements Resolve<any> {

   constructor(public dashboardService: DashboardService) { }

   resolve() {
      return this.dashboardService.getDateRange();
   }
}
