import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { DashboardService } from '../services/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'app/util/util';
import { NbComponentStatus, NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { environment } from 'environments/environment';
import { UnmaskDetailsModalComponent } from '../pages/unmask-details-modal/unmask-details-modal/unmask-details-modal.component';
import { NbWindowService } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class DashboardStore implements OnDestroy {
  apiURL = environment.apiURL;
  util = Util;
  cpId = null;
  dashboardTiles = null;
  wingaFacts = null;
  chartsData = null;
  seriesData = [];
  legendData = [];
  states: any = null;
  clients: any = null;
  campaignGroups: any = null;
  campaigns: any = null;
  clientData: any = null;
  campaignsData: any = null;
  adsData: any = null;
  userAdViewData: any = null;
  unmaskData: any = null;
  selectedUserAdViewRow: any = null;
  clientId: any = null;
  campaignId: any = null;
  adsList: any = null;
  adId: any = null;
  fromDate: any = null;
  toDate: any = null;
  month: any = null;

  shortSessions: any = null;
  ss_cgsId: any = null;
  shortSessionWinners: any = null;
  shortSessionParticipatedUsers: any = null;
  shortSessionSelectedRow = null;
  shortSessionUnmaskData = null;

  showSpinner: boolean = false;
  dateRangeDropDown = null;
  totals = {
    noOfUsersPlayed: 0,
    totalEngagementTime: 0,
    wingaLeads: 0,
  };

  currentDashboardView = null;

  selectedClient: any = null;
  selectedCampaignGroup: any = null;
  constructor(private http: HttpClient, private appStore: AppStore, private dashboardService: DashboardService,
    private router: Router, private activatedRoute: ActivatedRoute, private windowService: NbWindowService,
    private toastrService: NbToastrService ) {
      window.onbeforeunload = () => this.ngOnDestroy();
      if (localStorage.getItem('dashboard')) {
        const dashboard = JSON.parse(localStorage.getItem('dashboard'));
        this.dateRangeDropDown = dashboard.dateRangeDropDown;
        this.month = dashboard.month || dashboard.dateRangeDropDown[0]?.month;
        this.clientId = dashboard.clientId;
        this.campaignId = dashboard.campaignId;
        this.adId = dashboard.adId;
        this.ss_cgsId = dashboard.ss_cgsId;
      }
      this.cpId = this.appStore.user.cpId;
      if (this.cpId > 0) {
        this.clientId = this.cpId;
      }
  }
  ngOnDestroy() {
    const dashboard = {
      dateRangeDropDown: this.dateRangeDropDown,
      month: this.month,
      clientId: this.clientId,
      campaignId: this.campaignId,
      adId: this.adId,
      ss_cgsId: this.ss_cgsId,
    };
    localStorage.setItem('dashboard', JSON.stringify(dashboard));
  }
  resetValues() {
    // this.clientId = null;
    this.campaignId = null;
    this.adId = null;
    // this.dateRange = null;
    this.clientData = null;
    this.adsData = null;
    this.campaignsData = null;
    this.userAdViewData = null;
  }
  // getDateRange() {
  //   this.dashboardService.getDateRange().subscribe( dateRange => {
  //     this.dateRangeDropDown = dateRange;
  //     this.dateRange = dateRange[0];
  //   });
  // }
  getStates() {
    this.dashboardService.getStates().subscribe( states => {
      this.states = states;
    });
  }
  getClients() {
    this.dashboardService.getClients().subscribe( clients => {
      this.clients = clients;
    });
  }
  getCampaignGroups() {
    if (this.clientId) {
      this.dashboardService.getCampaignGroups(this.clientId).subscribe( campaignGroups => {
        this.campaignGroups = campaignGroups;
      });
    }
  }
  getCampaigns() {
    if (this.clientId) {
      this.dashboardService.getCampaigns(this.clientId).subscribe( campaigns => {
        this.campaigns = campaigns;
      });
    }
  }
  getDashboardTiles() {
    this.dashboardService.getDashboardTiles().subscribe( data => {
      this.dashboardTiles = data.widgets;
      this.wingaFacts = data.wingaQuickFacts;
      this.chartsData = data.charts;
    });
  }
  getCampaignsData() {
    this.getFromAndToDate();
    if (this.clientId && this.fromDate && this.toDate) {
      this.dashboardService.getCampaignsData(this.clientId, this.fromDate, this.toDate).subscribe( campaigns => {
        this.campaignsData = campaigns;
      });
    }
  }
  getCampaignsReport() {
    this.getFromAndToDate();
    if (this.clientId && this.fromDate && this.toDate) {
      this.dashboardService.getCampaignsReport(this.clientId, this.fromDate, this.toDate);
    }
  }
  getAdData() {
    this.getFromAndToDate();
    if (this.clientId && this.campaignId && this.fromDate && this.toDate) {
      this.showSpinner = true;
      this.dashboardService.getAdData(this.clientId, this.campaignId, this.fromDate, this.toDate).subscribe( campaigns => {
        this.adsData = campaigns;
        this.showSpinner = false;
      }, error => {
        this.showSpinner = false;
      });
    }
  }
getAdConsumptionReport() {
  this.getFromAndToDate();
  if (this.clientId && this.campaignId && this.fromDate && this.toDate) {
    this.dashboardService.getAdConsumptionReport(this.clientId, this.campaignId, this.fromDate, this.toDate);
  }
}
getUserContentViewsData() {
    this.getFromAndToDate();
    if (this.adId && this.fromDate && this.toDate) {
      this.showSpinner = true;
      this.dashboardService.getUserContentViewsData(this.adId, this.fromDate, this.toDate).subscribe( userAdViewData => {
        this.userAdViewData = userAdViewData;
        this.setIndex(this.userAdViewData);
        this.showSpinner = false;
      }, error => {
        this.showSpinner = false;
      });
    }
  }
  loadViewDetails(view) {
    this.currentDashboardView = view;
  }
  getClientData() {
    this.getFromAndToDate();
    if (this.fromDate && this.toDate) {
      this.dashboardService.getClientData(this.fromDate, this.toDate).subscribe( data => {
        this.clientData = data;
        this.getTotals();
      });
    }
  }
  getTotals() {
    for (const item of this.clientData.clientViewData) {
      this.totals.noOfUsersPlayed += item.noOfUsersPlayed;
      this.totals.totalEngagementTime += item.totalEngagementTime;
      this.totals.wingaLeads += item.wingaLeads;
  }
  }

  getFromAndToDate() {
    if (this.dateRangeDropDown) {
      this.dateRangeDropDown.forEach( element => {
        if (element.month === this.month) {
          this.fromDate = element.fromDate;
          this.toDate = element.toDate;
        }
      });
    }
  }
  getContentsByCampaignId() {
    this.dashboardService.getContentsByCampaignId(
      this.campaignId).subscribe( data => {
        this.adsList = data;
  });
  }
  getUnmaskData() {
    this.dashboardService.getUnmaskUserMobile(
        this.selectedUserAdViewRow.userId,
        this.cpId,
        this.appStore.user.username,
        this.appStore.user.clientUserId,
        this.fromDate,
        this.toDate).subscribe( data => {
      this.unmaskData = data;
      this.windowService.open(UnmaskDetailsModalComponent,
        { title: `User Details`, windowClass: 'profile-window', context: { data: data, selectedRow:  this.selectedUserAdViewRow} });
    }, error => {
      this.showToast('danger', 'Error', 'Internal server error');
      this.showSpinner = false;
    });
  }
  getShortSessions() {
    this.showSpinner = true;
    this.dashboardService.getShortSessions().subscribe( data => {
        this.shortSessions = data;
        this.showSpinner = false;
    });
  }
  getShortSessionWinners() {
    if (this.ss_cgsId) {
      this.showSpinner = true;
      this.shortSessionWinners = null;
      this.dashboardService.getShortSessionWinners(this.ss_cgsId).subscribe( data => {
        this.shortSessionParticipatedUsers = null;
        this.shortSessionWinners = data;
        this.setIndex(this.shortSessionWinners.shortSessionWinners);
        this.showSpinner = false;
      }, error => {
        this.showToast('danger', 'Error', 'Internal server error');
        this.showSpinner = false;
      });
    }
  }
  getShortSessionParticipatedUsers() {
    if (this.ss_cgsId) {
      this.showSpinner = true;
      this.shortSessionParticipatedUsers = null;
      this.dashboardService.getShortSessionParticipatedUsers(this.ss_cgsId).subscribe( data => {
          this.shortSessionWinners = null;
          this.shortSessionParticipatedUsers = data;
          this.setIndex(this.shortSessionParticipatedUsers.shortSessionWinners);
          this.showSpinner = false;
        }, error => {
          this.showToast('danger', 'Error', 'Internal server error');
          this.showSpinner = false;
        });
    }
  }
  getShortSessionUnmaskData() {
    const campaignId = this.shortSessionWinners ? this.shortSessionWinners.campaignId : this.shortSessionParticipatedUsers.campaignId;
    this.dashboardService.getShortSessionUnmaskUserMobile(
        this.shortSessionSelectedRow.userId,
        this.appStore.user.clientUserId,
        campaignId,
        this.appStore.user.username).subscribe( data => {
      this.shortSessionUnmaskData = data;
      this.windowService.open(UnmaskDetailsModalComponent,
        { title: `User Details`, windowClass: 'profile-window', context: { data: data, selectedRow:  this.shortSessionSelectedRow} });
    }, error => {
      this.showToast('danger', 'Error', 'Internal server error');
      this.showSpinner = false;
    });
  }
  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 5000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? ` ${title}` : '';
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
  setIndex(data) {
    data.forEach( ( element, index ) => element['index'] = index + 1 );
  }
}
