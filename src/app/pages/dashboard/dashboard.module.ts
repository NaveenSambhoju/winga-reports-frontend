import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-chartjs';
import { NgxEchartsModule } from 'ngx-echarts';
import { NbCardModule,
         NbButtonModule,
         NbIconModule,
         NbActionsModule,
         NbCheckboxModule,
         NbDatepickerModule,
         NbInputModule,
         NbRadioModule,
         NbSpinnerModule,
         NbContextMenuModule,
         NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {BreadcrumbModule} from 'xng-breadcrumb';
import { ThemeModule } from '../../@theme/theme.module';
import { DashboardMainComponent } from './main/dashboard-main.component';
import { DashboardFilterComponent } from './pages/dashboard-filter/dashboard-filter.component';
import { DashboardComponent } from './pages/dashboard-tiles/dashboard.component';
import { DashboardStore } from './store/dashboard-store.service';
import { DashboardService } from './services/dashboard.service';
import { NbMomentDateModule } from '@nebular/moment';
import { CampaignDetailsComponent } from './pages/campaign-details/campaign-details/campaign-details.component';
import { AdDetailsComponent } from './pages/ad-details/ad-details/ad-details.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ShortSessionDetailsComponent } from './pages/short-session-details/short-session-details/short-session-details.component';
import { UserContentViewsComponent } from './pages/user-content-views/user-content-views/user-content-views.component';
import { TableCustomRendererComponent } from './pages/table-custom-renderer/table-custom-renderer/table-custom-renderer.component';
import { UnmaskDetailsModalComponent } from './pages/unmask-details-modal/unmask-details-modal/unmask-details-modal.component';
import { TableCustomMenuRendererComponent } from './pages/table-custom-menu-renderer/table-custom-menu-renderer.component';
import { WinnerDetailsComponent } from './pages/winner-details/winner-details.component';
import { ParticipatedUsersComponent } from './pages/participated-users/participated-users.component';
import { DateRangeResolver } from './services/daterange-resolver';
import { AppConstants } from 'app/util/app-constants';
import { AreaStackChartComponent } from './pages/area-stack-chart/area-stack-chart.component';
import { ShortSessionMaskComponent } from './pages/short-session-mask/short-session-mask.component';
const BREADCRUMS = AppConstants.BREADCRUMS;
const routes: Routes = [{
  path: '',
  data: {
    breadcrumb: BREADCRUMS.dashboard.label,
  },
  component: DashboardMainComponent,
  resolve: {
    routeResolver: DateRangeResolver,
  },
  children: [
    {
        path: BREADCRUMS.clients_report.path,
        data: {
          breadcrumb: BREADCRUMS.clients_report.label,
        },
        children: [
          {
            path: '',
            component: DashboardFilterComponent,
          },
          {
            path: BREADCRUMS.campaigns_report.path,
            data: {
              breadcrumb: BREADCRUMS.campaigns_report.label,
            },
            children: [
              {
                path: '',
                component: CampaignDetailsComponent,
              },
              {
                path: BREADCRUMS.ad_consumption_report.path,
                data: {
                  breadcrumb: BREADCRUMS.ad_consumption_report.label,
                },
                children: [
                  {
                    path: '',
                    component: AdDetailsComponent,
                  },
                  {
                    path: BREADCRUMS.short_sessions.path,
                    data: {
                      breadcrumb: BREADCRUMS.short_sessions.label,
                    },
                    component: ShortSessionDetailsComponent,
                  },
                  {
                    path: BREADCRUMS.ad_views.path,
                    data: {
                      breadcrumb: BREADCRUMS.ad_views.label,
                    },
                    component: UserContentViewsComponent,
                  },
                ],
              },
            ],
          },
        ],
    },
    {
      path: BREADCRUMS.campaigns_report.path,
      data: {
        breadcrumb: BREADCRUMS.campaigns_report.label,
      },
      children: [
        {
          path: '',
          component: CampaignDetailsComponent,
        },
        {
          path: BREADCRUMS.ad_consumption_report.path,
          data: {
            breadcrumb: BREADCRUMS.ad_consumption_report.label,
          },
          children: [
            {
              path: '',
              component: AdDetailsComponent,
            },
            {
              path: BREADCRUMS.short_sessions.path,
              data: {
                breadcrumb: BREADCRUMS.short_sessions.label,
              },
              component: ShortSessionDetailsComponent,
            },
            {
              path: BREADCRUMS.ad_views.path,
              data: {
                breadcrumb: BREADCRUMS.ad_views.label,
              },
              component: UserContentViewsComponent,
            },
          ],
        },
      ],
    },
    {
      path: BREADCRUMS.ad_consumption_report.path,
      data: {
        breadcrumb: BREADCRUMS.ad_consumption_report.label,
      },
      children: [
        {
          path: '',
          component: AdDetailsComponent,
        },
        {
          path: BREADCRUMS.short_sessions.path,
          data: {
            breadcrumb: BREADCRUMS.short_sessions.label,
          },
          component: ShortSessionDetailsComponent,
        },
        {
          path: BREADCRUMS.ad_views.path,
          data: {
            breadcrumb: BREADCRUMS.ad_views.label,
          },
          component: UserContentViewsComponent,
        },
      ],
    },
    {
      path: BREADCRUMS.ad_views.path,
      data: {
        breadcrumb: BREADCRUMS.ad_views.label,
      },
      component: UserContentViewsComponent,
    },
    {
      path: BREADCRUMS.short_sessions.path,
      data: {
        breadcrumb: BREADCRUMS.short_sessions.label,
      },
      children: [
        {
          path: '',
          component: ShortSessionDetailsComponent,
        },
        {
          path: BREADCRUMS.winners_details.path,
          data: {
            breadcrumb: BREADCRUMS.winners_details.label,
          },
          component: WinnerDetailsComponent,
        },
        {
          path: BREADCRUMS.participated_users.path,
          data: {
            breadcrumb: BREADCRUMS.participated_users,
          },
          component: ParticipatedUsersComponent,
        },
      ],
    },
    {
        path: '',
        component: DashboardComponent,
    },
  ]},
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbCardModule,
    ThemeModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbSpinnerModule,
    NbMomentDateModule,
    ChartModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbContextMenuModule,
    BreadcrumbModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    DashboardComponent,
    DashboardFilterComponent,
    DashboardMainComponent,
    CampaignDetailsComponent,
    AdDetailsComponent,
    ShortSessionDetailsComponent,
    UserContentViewsComponent,
    TableCustomRendererComponent,
    UnmaskDetailsModalComponent,
    TableCustomMenuRendererComponent,
    WinnerDetailsComponent,
    ParticipatedUsersComponent,
    AreaStackChartComponent,
    ShortSessionMaskComponent,
  ],
  providers: [DashboardStore, DashboardService, DateRangeResolver],
})
export class DashboardModule { }
