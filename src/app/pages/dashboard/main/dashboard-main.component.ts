import { Component, OnInit } from '@angular/core';
import { DashboardStore } from '../store/dashboard-store.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'winga-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss'],
})
export class DashboardMainComponent implements OnInit {

  constructor(public dashboardStore: DashboardStore,  private actRoute: ActivatedRoute) {
    this.dashboardStore.getDashboardTiles();
   }

  ngOnInit(): void {
    this.actRoute.data.subscribe(data => {
      this.dashboardStore.dateRangeDropDown = data.routeResolver;
      if (!this.dashboardStore.month) {
        this.dashboardStore.month = data.routeResolver[0].month;
      }
    });
  }

}
