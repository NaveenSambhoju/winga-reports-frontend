import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'app/util/app-constants';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-winner-details',
  templateUrl: './winner-details.component.html',
  styleUrls: ['./winner-details.component.scss'],
})
export class WinnerDetailsComponent implements OnInit {
  settings = AppConstants.SHORT_SESSIONS_WINNERS_DETAILS_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore) {
    this.loadData();
  }

  ngOnInit(): void {
  }

  loadData() {
    this.dashboardStore.getShortSessionWinners();
  }
  rowClicked(event) {
  }
}
