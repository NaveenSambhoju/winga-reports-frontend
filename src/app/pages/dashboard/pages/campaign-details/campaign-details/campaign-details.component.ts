import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppConstants } from 'app/util/app-constants';
import { Util } from 'app/util/util';

@Component({
  selector: 'winga-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss'],
})
export class CampaignDetailsComponent implements OnInit {
  appConstants = AppConstants;
  util = Util;
  settings = AppConstants.CAMPAIGN_VIEW_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore, private router: Router, private activatedRoute: ActivatedRoute) {
    this.dashboardStore.campaignsData = null;
    this.dashboardStore.getClients();
    this.loadData();
   }

  ngOnInit(): void {
  }
  loadData() {
    this.dashboardStore.getCampaignsData();
  }
  downloadReport() {
    this.dashboardStore.getCampaignsReport();
  }
  rowClicked(event) {
    this.dashboardStore.campaignId = event.data.campaignId;
    this.router.navigate([AppConstants.BREADCRUMS.ad_consumption_report.path], {relativeTo: this.activatedRoute});
  }
}
