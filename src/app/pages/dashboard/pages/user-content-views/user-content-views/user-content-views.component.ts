import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppConstants } from 'app/util/app-constants';
import { Util } from 'app/util/util';

@Component({
  selector: 'winga-user-content-views',
  templateUrl: './user-content-views.component.html',
  styleUrls: ['./user-content-views.component.scss'],
})
export class UserContentViewsComponent implements OnInit {

  appConstants = AppConstants;
  util = Util;
  settings = AppConstants.USER_AD_VIEW_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore) {
    this.dashboardStore.adsData = null;
    this.dashboardStore.getClients();
    this.dashboardStore.getCampaigns();
    this.dashboardStore.getContentsByCampaignId();
    this.loadData();
   }

  ngOnInit(): void {
  }
  onClientChange() {
    this.dashboardStore.campaignId = null;
    this.dashboardStore.adId = null;
    this.dashboardStore.getCampaigns();
  }
  onCampaignChange() {
    this.dashboardStore.adId = null;
    this.dashboardStore.getContentsByCampaignId();
  }
  loadData() {
    this.dashboardStore.getUserContentViewsData();
  }
  rowClicked(event) {
  }

}
