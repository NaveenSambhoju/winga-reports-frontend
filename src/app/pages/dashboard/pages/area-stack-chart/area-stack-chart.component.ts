import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-area-stack-chart',
  templateUrl: './area-stack-chart.component.html',
  styleUrls: ['./area-stack-chart.component.scss'],
})
export class AreaStackChartComponent implements OnDestroy {

  options: any = {};
  themeSubscription: any;
  constructor( public dashboardStore: DashboardStore, private theme: NbThemeService) {
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
        const colors: any = config.variables;
        const echarts: any = config.variables.echarts;
        const legendData = [];
        const seriesData = [];
          this.dashboardStore.chartsData.data.forEach(element => {
            seriesData.push(
              {
                name: element.name,
                type: 'line',
                stack: 'Total amount',
                areaStyle: { normal: { opacity: echarts.areaOpacity } },
                data: element.data,
              },
            );
            legendData.push(element.name);
          });
        this.options = {
          backgroundColor: echarts.bg,
          color: [colors.warningLight, colors.infoLight, colors.dangerLight, colors.successLight, colors.primaryLight],
          tooltip: {
            trigger: 'axis',
            appendToBody: true,
            textStyle: {
              fontSize: 10,
            },
            axisPointer: {
              type: 'cross',
              label: {
                backgroundColor: echarts.tooltipBackgroundColor,
              },
            },
          },
          legend: {
            data: legendData,
            type: 'scroll',
            textStyle: {
              color: echarts.textColor,
            },
          },
          grid: {
            left: '3%',
            right: '4%',
            bottom: '3%',
            containLabel: true,
          },
          xAxis: [
            {
              type: 'category',
              boundaryGap: false,
              data: this.dashboardStore.chartsData.weeks,
              axisTick: {
                alignWithLabel: true,
              },
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          yAxis: [
            {
              type: 'value',
              axisLine: {
                lineStyle: {
                  color: echarts.axisLineColor,
                },
              },
              splitLine: {
                lineStyle: {
                  color: echarts.splitLineColor,
                },
              },
              axisLabel: {
                textStyle: {
                  color: echarts.textColor,
                },
              },
            },
          ],
          series: seriesData,
        };
    });
  }

  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }

}
