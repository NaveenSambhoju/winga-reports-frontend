import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaStackChartComponent } from './area-stack-chart.component';

describe('AreaStackChartComponent', () => {
  let component: AreaStackChartComponent;
  let fixture: ComponentFixture<AreaStackChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreaStackChartComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaStackChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
