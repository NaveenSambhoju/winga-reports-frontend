import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppConstants } from 'app/util/app-constants';
import { Util } from 'app/util/util';

@Component({
  selector: 'winga-ad-details',
  templateUrl: './ad-details.component.html',
  styleUrls: ['./ad-details.component.scss'],
})
export class AdDetailsComponent implements OnInit {
  appConstants = AppConstants;
  util = Util;
  settings = AppConstants.AD_VIEW_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore, private router: Router, private activatedRoute: ActivatedRoute) {
    this.dashboardStore.adsData = null;
    this.dashboardStore.getClients();
    this.dashboardStore.getCampaigns();
    this.loadData();
   }

  ngOnInit(): void {
  }
  onClientChange() {
    this.dashboardStore.campaignId = null;
    this.dashboardStore.getCampaigns();
  }
  loadData() {
    this.dashboardStore.getAdData();
  }
  downloadReport() {
    this.dashboardStore.getAdConsumptionReport();
  }
  rowClicked(event) {
    this.dashboardStore.adId = event.data.adId;
    this.router.navigate([AppConstants.BREADCRUMS.ad_views.path], {relativeTo: this.activatedRoute});
  }
}
