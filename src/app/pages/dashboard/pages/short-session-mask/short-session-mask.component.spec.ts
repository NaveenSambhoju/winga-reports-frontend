import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortSessionMaskComponent } from './short-session-mask.component';

describe('ShortSessionMaskComponent', () => {
  let component: ShortSessionMaskComponent;
  let fixture: ComponentFixture<ShortSessionMaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShortSessionMaskComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortSessionMaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
