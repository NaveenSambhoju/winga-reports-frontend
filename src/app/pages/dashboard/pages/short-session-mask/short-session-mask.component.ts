import { Component, Input } from '@angular/core';
// import { ViewCell } from 'ng2-smart-table';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppStore } from 'app/@core/store/app-store.service';

@Component({
  selector: 'winga-short-session-mask',
  templateUrl: './short-session-mask.component.html',
  styleUrls: ['./short-session-mask.component.scss'],
})
export class ShortSessionMaskComponent {
  @Input() value: any;    // This hold the cell value
  @Input() rowData: any;  // This holds the entire row object
  constructor(public dashboardStore: DashboardStore) {

  }
  unmask() {
    this.dashboardStore.shortSessionSelectedRow = this.rowData;
    this.dashboardStore.getShortSessionUnmaskData();
  }

}
