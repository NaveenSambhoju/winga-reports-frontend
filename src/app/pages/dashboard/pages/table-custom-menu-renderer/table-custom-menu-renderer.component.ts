import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbMenuService } from '@nebular/theme';
import { AppConstants } from 'app/util/app-constants';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-table-custom-menu-renderer',
  templateUrl: './table-custom-menu-renderer.component.html',
  styleUrls: ['./table-custom-menu-renderer.component.scss'],
})
export class TableCustomMenuRendererComponent implements OnInit, OnDestroy {
  @Input() value; // data from table
  @Input() rowData;
  data: any = null;
  $menuService: Subscription;
  items = [
    { title: AppConstants.SS_MENU_ITEM_WINNERS },
    { title: AppConstants.SS_MENU_ITEM_PARTICIPATED_USERS },
  ];
  constructor(private menuService: NbMenuService, private router: Router, private activatedRoute: ActivatedRoute,
    private dashboardStore: DashboardStore) {
    this.$menuService = this.menuService.onItemClick().pipe(
      filter(({ tag }) => tag === 'my-context-menu' + this.value),
      ).subscribe(( event ) => {
          if ( event.item.title === AppConstants.SS_MENU_ITEM_WINNERS ) {
            this.dashboardStore.ss_cgsId = this.rowData.cgsId;
            this.router.navigate([AppConstants.BREADCRUMS.winners_details.path], {relativeTo: this.activatedRoute});
          }
          if ( event.item.title === AppConstants.SS_MENU_ITEM_PARTICIPATED_USERS ) {
            this.router.navigate([AppConstants.BREADCRUMS.participated_users.path], {relativeTo: this.activatedRoute});
            this.dashboardStore.ss_cgsId = this.rowData.cgsId;
          }
        });
  }

  ngOnInit(): void {}
  ngOnDestroy() {
    this.$menuService.unsubscribe();
  }
}
