import { Component, Inject, OnInit } from '@angular/core';
import { NB_WINDOW_CONTEXT } from '@nebular/theme';

@Component({
  selector: 'winga-unmask-details-modal',
  templateUrl: './unmask-details-modal.component.html',
  styleUrls: ['./unmask-details-modal.component.scss'],
})
export class UnmaskDetailsModalComponent implements OnInit {
  modalData;
  constructor(@Inject(NB_WINDOW_CONTEXT) modalData) {
    this.modalData = modalData;
   }

  ngOnInit(): void {
  }

}
