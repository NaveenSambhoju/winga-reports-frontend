import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from 'app/util/app-constants';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
              public dashboardStore: DashboardStore) {
  }
  viewDetails(view) {
    const BREADCRUMS = AppConstants.BREADCRUMS;
    this.dashboardStore.loadViewDetails(view);
    this.dashboardStore.resetValues();
    switch (view) {
      case 'Clients' : this.router.navigate([BREADCRUMS.clients_report.path], {relativeTo: this.activatedRoute}); break;
      case 'Campaigns' : this.router.navigate([BREADCRUMS.campaigns_report.path], {relativeTo: this.activatedRoute}); break;
      case 'Ad View Count' : this.router.navigate([BREADCRUMS.ad_consumption_report.path], {relativeTo: this.activatedRoute}); break;
      case 'Users' : this.router.navigate([BREADCRUMS.ad_views.path],
                    {relativeTo: this.activatedRoute}); break;
      case 'Short Sessions' : this.router.navigate([BREADCRUMS.short_sessions.path],
                              {relativeTo: this.activatedRoute}); break;
    }
  }
}
