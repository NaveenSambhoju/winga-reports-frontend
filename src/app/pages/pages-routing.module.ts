import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { AppConstants } from 'app/util/app-constants';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: AppConstants.BREADCRUMS.dashboard.path,
      loadChildren: () => import ('../pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    },
    {
      path: 'charts',
      loadChildren: () => import ('../features/charts/charts.module').then(m => m.ChartsModule),
    },
    {
      path: 'table',
      loadChildren: () => import ('../features/table/tables.module').then(m => m.TablesModule),
    },
    {
      path: '',
      redirectTo: AppConstants.BREADCRUMS.dashboard.path,
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
