import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppStore } from 'app/@core/store/app-store.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { AppURLS } from 'app/util/app.urls';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  apiURL = environment.apiURL;
  loginErrorMessage = null;
  showSpinner = false;
  constructor(private http: HttpClient, private appStore: AppStore, private router: Router) { }

  login(username, password) {
    this.loginErrorMessage = null;
    this.showSpinner = true;
    this.http.post<any>(`${this.apiURL}${AppURLS.login}`, { username, password })
        .subscribe( user => {
            localStorage.clear();
            localStorage.setItem('user', JSON.stringify(user));
            this.appStore.user = user.user;
            this.appStore.token = user.tokens['access-token'];
            this.showSpinner = false;
            this.router.navigate(['pages/dashboard']);
        }, error => {
          this.loginErrorMessage = error.error.message;
          this.showSpinner = false;
          localStorage.clear();
        });
}

  logout() {
    localStorage.clear();
    // this.router.navigate(['pages/login']);
      window.location.reload();
  }
}
