import { ChangeDetectorRef, Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogService } from '@nebular/theme';
import { LoginService } from './login.service';

@Component({
  selector: 'winga-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    protected options: {};
    protected cd: ChangeDetectorRef;
    redirectDelay: number;
    showMessages: any;
    strategy: string;
    errors: string[];
    messages: string[];
    user: any = {username: '', password: '', rememberMe: false};
    submitted: boolean;
    rememberMe: boolean;
    constructor(cd: ChangeDetectorRef, public router: Router, private dialogService: NbDialogService, public loginService: LoginService) {

    }
    ngOnInit() {}
    login() {
      this.loginService.login(this.user.username, this.user.password);
    }
    getConfigValue(key: string) {}
    // static ɵfac: ɵngcc0.ɵɵFactoryDeclaration<NbLoginComponent, never>;
    // static ɵcmp: ɵngcc0.ɵɵComponentDeclaration<NbLoginComponent, "nb-login", never, {}, {}, never, never>;
    openWithoutBackdropClick(dialog: TemplateRef<any>) {
      this.dialogService.open(
        dialog,
        {
          context: 'this is some additional data passed to dialog',
          closeOnBackdropClick: false,
        });
    }

}
