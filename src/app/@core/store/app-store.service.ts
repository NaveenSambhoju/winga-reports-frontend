import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppStore {
  theme: String =  null;
  user: any = null;
  token: any = null;
  constructor() { }
}
